<?php

namespace Splynx\Sniffs\comments;

use PHP_CodeSniffer\Sniffs\Sniff;

class SingleLineCommentsCheckSniff implements Sniff
{
    /** @var  \PHP_CodeSniffer\Fixer $_fixer */
    private $_fixer;

    /** @var int|null $_position */
    private $_position;

    /** @var string|null $_errorType */
    private $_errorType;

    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function register()
    {
        return [T_COMMENT];
    }

    public function process(\PHP_CodeSniffer\Files\File $phpcsFile, $stackPtr): void
    {
        $this->_fixer = $phpcsFile->fixer;
        $this->_position = $stackPtr;

        $tokens = $phpcsFile->getTokens();

        $content = $tokens[$stackPtr]['content'];

        if (substr($content, 0, 2) !== '//' || trim($content) === '//') {
            return;
        }

        $re = '/^..(.)(.+).*(.$)/m';
        preg_match_all($re, $content, $matches, PREG_SET_ORDER, 0);
        if (!isset($matches[0])) {
            $error = 'Wrong single line comment %s';
            $data = [trim($tokens[$stackPtr]['content'])];
            $phpcsFile->addError($error, $stackPtr, 'Found', $data);
            return;
        }

        $matches = $matches[0];
        $firstSymbolAfterWhitespace = $matches[1] !== ' ' ? $matches[1] : substr(trim($matches[2]), 0, 1);

        if ($matches[1] === '@' || $firstSymbolAfterWhitespace === '@') {
            return;
        }

        if ($matches[1] !== ' ') {
            $error = 'Single line comments should be started with whitespace %s';
            $data = [trim($tokens[$stackPtr]['content'])];
            $phpcsFile->addFixableError($error, $stackPtr, 'Found', $data);
            $this->_errorType = 'whitespace';
            $this->fix();
        }

        // Check if previus comment ends with a comma.
        $prevComa = false;
        $token = null;
        if (isset($tokens[$stackPtr - 1])) {
            $token = $tokens[$stackPtr - 1];
            $prevContent = trim($token['content']);
            if (!$prevContent) {
                if (isset($tokens[$stackPtr - 2])) {
                    $token = $tokens[$stackPtr - 2];
                    $prevContent = trim($token['content']);
                }
            }
            $prevLastChar = substr(trim($prevContent), -1, 1);

            if (($prevLastChar === ',' || $prevLastChar === ':') && $token['code'] === T_COMMENT) {
                $prevComa = true;
            }
        }

//        $firstCharacter = $matches[1] !== ' ' ? $matches[1] : $matches[2];
        if ($firstSymbolAfterWhitespace !== strtoupper($firstSymbolAfterWhitespace) && !$prevComa) {
            $error = 'Single line comments should be started with uppercase %s';
            $data = [trim($tokens[$stackPtr]['content'])];
            $phpcsFile->addFixableError($error, $stackPtr, 'Found', $data);
            $this->_errorType = 'uppercase';
            $this->fix();
        }
//        $lastCharacter = $matches[3];

        /*if ($lastCharacter !== '.') {
            $error = 'Single line comments should end with dot %s';
            $data = [trim($tokens[$stackPtr]['content'])];
            $phpcsFile->addFixableError($error, $stackPtr, 'Found', $data);
            $this->_errorType = 'end_dot';
            $this->fix();
        }*/
    }

    private function fix(): void
    {
        if ($this->_errorType === 'whitespace') {
            $content = $this->_fixer->getTokenContent($this->_position);
            $newContent = substr_replace($content, ' ', 2, 0);
            $this->_fixer->replaceToken($this->_position, $newContent);
        }
        /*if ($this->_errorType === 'oneWhitespace') {
            $content = $this->_fixer->getTokenContent($this->_position);
            $newContent = substr_replace($content, '', 3, 1);
            $this->_fixer->replaceToken($this->_position, $newContent);
        }*/
        if ($this->_errorType === 'uppercase') {
            $content = $this->_fixer->getTokenContent($this->_position);
            $firstCharacter = substr(trim(substr($content, 3)), 0, 1);
            $firstStringPosition = strpos($content, $firstCharacter);
            $firstCharacter = strtoupper($firstCharacter);
            $newContent = substr_replace($content, $firstCharacter, $firstStringPosition, 1);
            $this->_fixer->replaceToken($this->_position, $newContent);
        }
        /*if ($this->_errorType === 'end_dot') {
            $content = $this->_fixer->getTokenContent($this->_position);
            $newContent = substr_replace($content, '.', -1, 0);
            $this->_fixer->replaceToken($this->_position, $newContent);
        }*/
    }
}
