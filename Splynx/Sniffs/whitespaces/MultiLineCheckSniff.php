<?php

namespace Splynx\Sniffs\whitespaces;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

class MultiLineCheckSniff implements Sniff
{
    /** @var  \PHP_CodeSniffer\Fixer $_fixer */
    private $_fixer;

    /** @var int|null $_position */
    private $_position;

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_WHITESPACE];
    }

    /**
     * @inheritDoc
     */
    public function process(File $phpcsFile, $stackPtr): void
    {
        $this->_fixer = $phpcsFile->fixer;
        $this->_position = $stackPtr;

        $tokens = $phpcsFile->getTokens();

        if (!isset($tokens[$stackPtr - 1]) || !isset($tokens[$stackPtr - 2])) {
            return;
        }
        $content = $tokens[$stackPtr]['content'];
        $prevLastChar = substr($tokens[$stackPtr - 1]['content'], -1, 1);
        $prevThirdLastChar = substr($tokens[$stackPtr - 2]['content'], -1, 1);

        if ($content !== PHP_EOL || $prevLastChar !== PHP_EOL || $tokens[$stackPtr - 1]['code'] !== T_WHITESPACE || $prevThirdLastChar !== PHP_EOL) {
            return;
        }

        $error = 'Line break must be no more than 3 in a row %s';
        $data = [trim($tokens[$stackPtr]['content'])];
        $phpcsFile->addFixableError($error, $stackPtr, 'Found', $data);
        $this->fix();
    }

    private function fix(): void
    {
        $this->_fixer->replaceToken($this->_position, '');
    }
}
