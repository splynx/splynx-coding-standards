Splynx Coding Standard
===================================

Based on [Yii 2 Web Framework Coding Standard](https://github.com/yiisoft/yii2-coding-standards)


Installation
===================================
Add to your `composer.json`:

```
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:splynx/splynx-coding-standards.git"
        }
    ],
    "require-dev": {
        "splynx/splynx-coding-standards": "dev-master"
    }
```

Or you can install this repo separate

How to setup
===================================

Go to "Languages & Frameworks > PHP > Quality tools > Code Sniffer". Select "Local" and push "...". Find and select file "__DIR__/vendor/bin/phpcs". Push "Validate" to make shure you selection is right.

Go to "Editor > Inspections". Find "PHP Code Sniffer validation", enable it. In right section in dropdown menu "Coding standart" select "Custom" and push "...". Select file "__DIR__/vendor/splynx/splynx-coding-standards/Splynx/ruleset.xml"

To run inspection in file press Alt+Shift+I